module gitlab.oit.duke.edu/devil-ops/go-patchmonkey

go 1.22.0

toolchain go1.23.2

require (
	github.com/Khan/genqlient v0.7.0
	github.com/golang-jwt/jwt/v5 v5.2.1
	github.com/suessflorian/gqlfetch v0.6.0
	gitlab.oit.duke.edu/devil-ops/go-iam-oidc v0.3.0
)

require (
	github.com/agnivade/levenshtein v1.2.0 // indirect
	github.com/alexflint/go-arg v1.5.1 // indirect
	github.com/alexflint/go-scalar v1.2.0 // indirect
	github.com/gopherjs/gopherjs v1.17.2 // indirect
	github.com/jtolds/gls v4.20.0+incompatible // indirect
	github.com/kr/pretty v0.3.0 // indirect
	github.com/smarty/assertions v1.16.0 // indirect
	github.com/vektah/gqlparser v1.3.1 // indirect
	github.com/vektah/gqlparser/v2 v2.5.17 // indirect
	golang.org/x/mod v0.21.0 // indirect
	golang.org/x/sync v0.8.0 // indirect
	golang.org/x/tools v0.26.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	moul.io/http2curl v1.0.0 // indirect
)
