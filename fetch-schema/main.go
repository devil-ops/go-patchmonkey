/*
Package main is the executable
*/
package main

import (
	"context"
	"fmt"
	"net/http"

	"github.com/suessflorian/gqlfetch"
	"gitlab.oit.duke.edu/devil-ops/go-iam-oidc/authenticate"
)

func main() {
	sltc := authenticate.NewShortLivedTokenConfig(
		authenticate.WithClientID("patchmonkey"),
		authenticate.WithEndpoints("/graphql"),
		authenticate.WithLLTEnv("PATCHMONKEY_TOKEN"),
	)
	ti, _, err := authenticate.ShortLivedToken(sltc)
	if err != nil {
		panic(err)
	}
	schema, err := gqlfetch.BuildClientSchemaWithHeaders(context.Background(), "https://patchmonkey.oit.duke.edu/graphql", http.Header{
		"Authorization": []string{"Bearer " + ti.Token},
	}, false)
	if err != nil {
		panic(err)
	}
	fmt.Println(schema)
}
