//go:generate go run github.com/Khan/genqlient
/*
Package patchmonkey provides the base bits for interacting with the Patchmonkey GraphQL API
*/
package patchmonkey

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"sync"
	"time"

	"github.com/Khan/genqlient/graphql"

	"gitlab.oit.duke.edu/devil-ops/go-iam-oidc/authenticate"
)

const (
	baseURL = "https://patchmonkey.oit.duke.edu"
)

// Client holds the base info used for interactions
type Client struct {
	UserAgent  string
	slt        *string
	sltRenewal *time.Time
	llt        string
	baseURL    string
	tokener    authenticate.Tokener
	gqlClient  graphql.Client
	mu         sync.Mutex
}

// SLT Returns the short lived token string
func (c *Client) SLT() string {
	return *c.slt
}

// updateSLT sets the Short lived token to the given value
func (c *Client) updateSLT(t string, d time.Time) {
	c.mu.Lock()
	c.slt = &t
	c.sltRenewal = &d
	slog.Debug("updated short lived token from IAM", "next-update", -time.Since(d))
	c.mu.Unlock()
}

// MakeRequest fulfills the interface for a graphql.Client interface.
func (c *Client) MakeRequest(ctx context.Context, req *graphql.Request, resp *graphql.Response) error {
	return c.gqlClient.MakeRequest(ctx, req, resp)
}

type authedTransport struct {
	wrapped http.RoundTripper
	client  *Client
}

func (t *authedTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	if *t.client.slt == "" {
		return nil, errors.New("must set slt")
	}
	req.Header.Set("Authorization", "Bearer "+*t.client.slt)
	return t.wrapped.RoundTrip(req)
}

// WithTokener specifies a new authenticate.Tokener interface
func WithTokener(tr authenticate.Tokener) func(*Client) {
	return func(c *Client) {
		c.tokener = tr
	}
}

// WithLLT creates a new client with the given long lived token
func WithLLT(t string) func(*Client) {
	return func(c *Client) {
		c.llt = t
	}
}

// WithBaseURL changes the base url of all the client calls
func WithBaseURL(u string) func(*Client) {
	return func(c *Client) {
		c.baseURL = u
	}
}

// WithSLT sets a Short Lived Token for use with authentication. Useful for
// testing so we don't have to fetch a legit one
func WithSLT(t string) func(*Client) {
	return func(c *Client) {
		c.slt = &t
	}
}

// WithGQLClient sets a custom http.Client for the GraphQL client
func WithGQLClient(hc graphql.Client) func(*Client) {
	return func(c *Client) {
		c.gqlClient = hc
	}
}

func (c *Client) refreshTokens() {
	for {
		if (c.sltRenewal != nil) && !time.Now().UTC().After(*c.sltRenewal) {
			time.Sleep(time.Second * 10)
		} else {
			slt, err := c.tokener.Token(authenticate.NewShortLivedTokenConfig(
				authenticate.WithClientID("patchmonkey"),
				authenticate.WithEndpoints("/graphql"),
				authenticate.WithLLT(c.llt),
				authenticate.WithLLTEnv("PATCHMONKEY_TOKEN"),
			))
			if err != nil {
				backOff(time.Second*10, "could not get a new token", err)
				continue
			}
			expires, err := slt.Expires()
			if err != nil {
				backOff(time.Second*10, "could not get a token expiration", err)
				continue
			}
			renewIn := -time.Since(*expires) / 2
			c.updateSLT(slt.Token, time.Now().UTC().Add(renewIn))
			time.Sleep(renewIn)
		}
	}
}

func backOff(d time.Duration, t string, e error) {
	slog.Warn(t, "error", e)
	time.Sleep(d)
}

// New creates a new client using functional options. Will panic if something goes awry
func New(opts ...func(*Client)) *Client {
	c := &Client{
		tokener:   authenticate.GetClient(),
		baseURL:   baseURL,
		UserAgent: "go-patchmonkey",
	}

	for _, o := range opts {
		o(c)
	}

	go c.refreshTokens()
	// Give it a sec to get the initial token
	time.Sleep(time.Second * 1)

	// If we never set a gqlClient, set up the default one here.
	if c.gqlClient == nil {
		// Jam in the graphql client
		dhc := http.DefaultClient
		dhc.Transport = &authedTransport{
			wrapped: http.DefaultTransport,
			client:  c,
		}
		c.gqlClient = graphql.NewClient(fmt.Sprintf("%v/graphql", c.baseURL), dhc)
	}

	return c
}
