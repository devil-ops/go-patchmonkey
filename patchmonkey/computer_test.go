package patchmonkey

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetComputerIDWithName(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(200)
		w.Write([]byte(`{
			"data": {
			  "computers": [
			    {
			      "name": "drews-dev-01.example.com",
			      "id": "16602"
			    }
			  ]
			}
		      }`))
	}))
	c := New(WithBaseURL(srv.URL), WithTokener(ttc))
	got, err := GetComputerIDWithName(context.TODO(), c, "drews-dev-01.example.com")
	requireNoError(t, err)
	assertEqual(t, "16602", got)
}

func TestGetComputerIDWithNameFail(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(500)
	}))
	c := New(WithBaseURL(srv.URL), WithTokener(ttc))
	got, err := GetComputerIDWithName(context.TODO(), c, "drews-dev-01.example.com")
	requireError(t, err, "returned error 500 Internal Server Error: ")
	assertEqual(t, "", got)
}

func TestGetComputerIDWithNameMultiple(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(200)
		w.Write([]byte(`{
			"data": {
			  "computers": [
			    {
			      "name": "drews-dev-01.example.com",
			      "id": "16602"
			    },
			    {
			      "name": "drews-dev-02.example.com",
			      "id": "16603"
			    }
			  ]
			}
		      }`))
	}))
	_, err := GetComputerIDWithName(context.TODO(), New(WithBaseURL(srv.URL), WithTokener(ttc)), "drews-dev")
	requireError(t, err, "multiple computers found")
}

func TestGetComputerIDWithNameEmpty(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(200)
		w.Write([]byte(`{
			"data": {
			  "computers": [ ]
			}
		      }`))
	}))
	_, err := GetComputerIDWithName(context.TODO(), New(WithBaseURL(srv.URL), WithTokener(ttc)), "drews-dev")
	requireError(t, err, "computer not found")
}
