package patchmonkey

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"
)

// MyInfo represents the response to the /my-info unauthenticated endpoint
type MyInfo struct {
	Name                  string     `json:"name,omitempty"`
	Production            bool       `json:"production,omitempty"`
	PatchWindowName       string     `json:"patch_window_name,omitempty"`
	LastPatched           *time.Time `json:"last_patched,omitempty"`
	LastSuccessfulPatch   *time.Time `json:"last_successful_patch,omitempty"`
	NextScheduledPatchRun *time.Time `json:"next_scheduled_patch_run,omitempty"`
}

// GetMyInfo returns a MyInfo object from the given url. If url is empty,
// default to {baseURL}/my-info
func GetMyInfo(u string) (*MyInfo, error) {
	if u == "" {
		u = fmt.Sprintf("%v/my_info", baseURL)
	}

	resp, err := http.Get(u) // nolint:gosec
	if err != nil {
		return nil, err
	}
	defer dclose(resp.Body)

	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("got a %v instead of a 200 response from patchmonkey", resp.StatusCode)
	}

	var mi MyInfo
	if err = json.NewDecoder(resp.Body).Decode(&mi); err != nil {
		return nil, err
	}

	return &mi, nil
}

func dclose(c io.Closer) {
	err := c.Close()
	if err != nil {
		panic(err)
	}
}
