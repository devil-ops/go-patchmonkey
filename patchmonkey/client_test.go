package patchmonkey

import (
	"net/http"
	"os"
	"testing"

	"github.com/golang-jwt/jwt/v5"

	"gitlab.oit.duke.edu/devil-ops/go-iam-oidc/authenticate"
)

// tsrv *httptest.Server     // test Token server
var ttc authenticate.Tokener // test token client

type fakeTokener struct {
	token *authenticate.TokenInfo
}

func (f fakeTokener) Token(_ *authenticate.ShortLivedTokenConfig) (*authenticate.TokenInfo, error) {
	return f.token, nil
}

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	shutdown()
	os.Exit(code)
}

func setup() {
	ttc = fakeTokener{
		token: &authenticate.TokenInfo{
			Token:  "bunk",
			Claims: jwt.MapClaims{"exp": float64(1728050509)},
		},
	}
}

func shutdown() {
	// tsrv.Close()
}

func TestNewClientEnvVars(t *testing.T) {
	tests := map[string]struct {
		vars      map[string]string
		wantPanic bool
	}{
		"should not error if PATCHMONKEY_TOKEN is set": {
			vars: map[string]string{
				"PATCHMONKEY_TOKEN": "test",
			},
			wantPanic: false,
		},
	}
	for desc, tt := range tests {
		for k, v := range tt.vars {
			t.Setenv(k, v)
		}
		if tt.wantPanic {
			assertPanics(t, func() { New(WithTokener(ttc)) }, desc)
		} else {
			assertNotPanics(t, func() { New(WithTokener(ttc)) }, desc)
		}
	}
}

func TestRoundTripper(t *testing.T) {
	c := New(WithTokener(ttc))
	requireNotNil(t, c)
	at := authedTransport{
		wrapped: http.DefaultTransport,
		client:  c,
	}
	req, err := http.NewRequest("GET", "https://www.example.com", nil)
	requireNoError(t, err)
	resp, err := at.RoundTrip(req)
	requireNoError(t, err)
	requireNotNil(t, resp)
	requireNoError(t, resp.Body.Close())
}

func TestNew(t *testing.T) {
	c := New(WithTokener(ttc))
	requireNotNil(t, c)
	tok, err := c.tokener.Token(&authenticate.ShortLivedTokenConfig{})
	requireNoError(t, err)
	assertNotEqual(t, "", tok.Token)
	// require.Implements(t, (*graphql.Client)(nil), c)
}

func requireNotNil(t *testing.T, v any) {
	if v == nil {
		t.Fatalf("got nil when expected not nil")
	}
}

func requireNoError(t *testing.T, err error) {
	if err != nil {
		t.Fatalf("expected no error, but got: %v", err)
	}
}

func requireError(t *testing.T, err error, expect string) {
	if err == nil {
		t.Fatalf("expected an error but got none")
	}
	if err.Error() != expect {
		t.Fatalf("expected '%v' error, but got: '%v'", expect, err)
	}
}

func assertNotEqual(t *testing.T, a, b string) {
	if a == b {
		t.Errorf("expected %v to be different from %v", a, b)
	}
}

func assertEqual(t *testing.T, a, b string) {
	if a != b {
		t.Errorf("expected %v to be %v", a, b)
	}
}

func assertPanics(t *testing.T, f func(), desc string) {
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic: %v", desc)
		}
	}()
	f()
}

func assertNotPanics(t *testing.T, f func(), desc string) {
	defer func() {
		if r := recover(); r != nil {
			t.Errorf("The code panic'd: %v", desc)
		}
	}()
	f()
}
