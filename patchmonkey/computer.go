package patchmonkey

import (
	"context"
	"errors"

	"github.com/Khan/genqlient/graphql"
)

// GetComputerIDWithName returns a computer ID when given a name
func GetComputerIDWithName(ctx context.Context, c graphql.Client, name string) (string, error) {
	var possibles []SearchComputerIdsWithNameComputersComputer
	r, err := SearchComputerIdsWithName(ctx, c, name)
	if err != nil {
		return "", err
	}
	possibles = append(possibles, r.Computers...)
	if len(possibles) > 1 {
		return "", errors.New("multiple computers found")
	} else if len(possibles) == 0 {
		return "", errors.New("computer not found")
	}
	return possibles[0].Id, nil
}
