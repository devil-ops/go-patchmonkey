package patchmonkey

import (
	"bytes"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestMyInfo(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		rp, err := os.ReadFile("./testdata/my-info.json")
		if err != nil {
			panic(err)
		}
		_, err = io.Copy(w, bytes.NewReader(rp))
		if err != nil {
			panic(err)
		}
	}))

	got, err := GetMyInfo(srv.URL)
	requireNoError(t, err)
	requireNotNil(t, got)
	assertEqual(t, "foo.example.com", got.Name)
	requireNotNil(t, got.LastPatched)
}
