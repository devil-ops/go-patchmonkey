package types

import (
	"testing"
	"time"
)

func TestParsePatchmonkeyDate(t *testing.T) {
	tests := []struct {
		date    string
		msg     string
		wantErr bool
	}{
		{
			date:    "2022-08-05T12:49:44-04:00",
			msg:     "full datetime should return a non-zero time",
			wantErr: false,
		},
		{
			date:    "2022-08-05",
			msg:     "full date should return a non-zero time",
			wantErr: false,
		},
		{
			date:    "not-a-date",
			msg:     "bad date should produce an error",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		_, err := parsePatchmonkeyDate(tt.date)
		if tt.wantErr {
			if err == nil {
				t.Errorf("expected an error, but didn't get one: %v", tt.msg)
			}
		} else {
			if err != nil {
				t.Errorf("didn't expect an error, but got one: %v : %v", tt.msg, err)
			}
		}
	}
}

func TestMarshalDate(t *testing.T) {
	ts := time.Date(2021, time.Month(2), 21, 1, 10, 30, 0, time.UTC)
	got, err := MarshalDate(ts)
	if err != nil {
		t.Fatalf("got an unexpected error: %v", err)
	}
	if string(got) != "2021-02-21 01:10:30 +0000 UTC" {
		t.Errorf("invalid timestamp from marshal")
	}
}

func TestUnmarshalDate(t *testing.T) {
	var target time.Time
	if err := UnmarshalDate([]byte("2010-03-04"), &target); err != nil {
		t.Fatalf("unexpected error encountered: %v", err)
	}
	if target != time.Date(2010, time.March, 4, 0, 0, 0, 0, time.UTC) {
		t.Errorf("unexpected time found during unmarshal")
	}
}

func TestUnmarshalDateErr(t *testing.T) {
	var target time.Time
	if err := UnmarshalDate([]byte("not a date"), &target); err == nil {
		t.Errorf("expected an error but didn't get one")
	}
}

func TestUnmarshalDateNil(t *testing.T) {
	var target time.Time
	if err := UnmarshalDate([]byte("null"), &target); err != nil {
		t.Errorf("expected no error, but got one: %v", err)
	}
}
