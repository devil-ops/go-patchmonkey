/*
Package types is a helper package that holds some misc. Type information needed by Patchmonkey
*/
package types

import (
	"errors"
	"fmt"
	"strings"
	"time"
)

func parsePatchmonkeyDate(s string) (time.Time, error) {
	var parsed *time.Time
	for _, f := range []string{"2006-01-02T15:04:05-07:00", "2006-01-02"} {
		got, err := time.Parse(f, s)
		if err == nil {
			parsed = &got
			break
		}
	}
	if (parsed == nil) || parsed.IsZero() {
		return time.Time{}, errors.New("could not parse date")
	}
	return *parsed, nil
}

// MarshalDate is the override for Marshaling of the Date
func MarshalDate(v fmt.Stringer) ([]byte, error) {
	return []byte(v.String()), nil
}

// UnmarshalDate is the override for Unmarshalling of the Date
func UnmarshalDate(b []byte, v *time.Time) error {
	s := strings.Trim(string(b), "\"")
	if s == "null" {
		return nil
	}
	t, err := parsePatchmonkeyDate(s)
	if err != nil {
		return err
	}
	*v = t
	return nil
}

/*
func destroy(p **time.Time) {
	*p = nil
}
*/
