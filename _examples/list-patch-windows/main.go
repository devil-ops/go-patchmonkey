package main

import (
	"context"
	"fmt"

	"gitlab.oit.duke.edu/devil-ops/go-patchmonkey/patchmonkey"
	"gopkg.in/yaml.v2"
)

func main() {
	c := patchmonkey.New()
	r, err := patchmonkey.PatchWindows(context.Background(), *c)
	if err != nil {
		panic(err)
	}
	d, err := yaml.Marshal(r)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(d))
}
