package main

import (
	"context"
	"fmt"
	"os"

	"gitlab.oit.duke.edu/devil-ops/go-patchmonkey/patchmonkey"
	"gopkg.in/yaml.v3"
)

func main() {
	c := patchmonkey.MustNewClient(nil, os.Getenv("PATCHMONKEY_TOKEN"), nil)
	var id string
	var d []byte
	var err error
	if len(os.Args) > 1 {
		id, err = patchmonkey.GetComputerIDWithName(context.Background(), *c, os.Args[1])
		if err != nil {
			panic(err)
		}
		r, err := patchmonkey.ComputersWithId(context.Background(), *c, []string{id})
		if err != nil {
			panic(err)
		}
		d, err = yaml.Marshal(r)
		if err != nil {
			panic(err)
		}
	} else {
		// r, err := patchmonkey.ComputersWithId(context.Background(), *c, []string{"16602"})
		r, err := patchmonkey.Computers(context.Background(), *c)
		if err != nil {
			panic(err)
		}
		d, err = yaml.Marshal(r)
		if err != nil {
			panic(err)
		}
	}
	fmt.Println(string(d))
}
