package main

import (
	"context"
	"fmt"
	"os"

	"gitlab.oit.duke.edu/devil-ops/go-patchmonkey/patchmonkey"
	"gopkg.in/yaml.v2"
)

func main() {
	if len(os.Args) != 2 {
		panic(fmt.Sprintf("Usage: %v COMPUTER_NAME", os.Args[0]))
	}
	c := patchmonkey.New()
	r, err := patchmonkey.ConnectionTest(context.Background(), *c, os.Args[1])
	if err != nil {
		panic(err)
	}
	d, err := yaml.Marshal(r)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(d))
}
