# Go Patchmonkey

 [![coverage report](https://gitlab.oit.duke.edu/devil-ops/go-patchmonkey/badges/main/coverage.svg)](https://gitlab.oit.duke.edu/devil-ops/go-patchmonkey/-/commits/main)

## Description

Golang library for interacting with the Patchmonkey API. Docs on the API can be
found [here](https://patchmonkey.oit.duke.edu/help/graphql)

## Usage

Right now just check out the stuff in `_examples/`. Still trying to figure out
how this all should work.

For the examples to work, export the `PATCHMONKEY_TOKEN` env var to your
long lived token.

## Auto Generated GraphQL Code

Code is generated from a schema using [Genqlient](https://github.com/Khan/genqlient).

Be sure to run `go generate ./...` after making changes to the schema or query
listings.

```
go run ./fetch-schema/ > patchmonkey/schema.graphql
go generate ./...
```

## Testing Tips

Check with http calls using something like this:

```bash
$ cat /tmp/gql.json
{
  "query": "query Computers($name_search: String!, $show_retired: Boolean!) { computers(nameSearch: $name_search, showRetired: $show_retired) { name } }",
  "operationName": "Computers",
  "variables": {
    "name_search": "drews",
    "show_retired": false
  }
}
$ curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $(idms-oidc-tool get-shortlived-token -c 'patchmonkey' -e '/graphql' -t)" https://patchmonkey.oit.duke.edu/graphql -d @/tmp/gql.json
...
```
